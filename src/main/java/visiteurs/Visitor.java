package visiteurs;

import models.Accessoire;
import models.ConnecteurX;
import models.ConnecteurY;
import models.Gouteur;

public interface Visitor {
    void visit(Gouteur g);
    void visit(ConnecteurY cy);
    void visit(ConnecteurX cx);
}
