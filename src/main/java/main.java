import models.Accessoire;
import models.ConnecteurX;
import models.ConnecteurY;
import models.Gouteur;
import visiteurs.VisiteurConcret;

public class main {
    public static void main(String[] args) {
        Accessoire accessoire = new ConnecteurY(
                new ConnecteurX(
                        new Gouteur(),
                        new Gouteur(),
                        new Gouteur()
                ),
                new ConnecteurX(
                        new Gouteur(),
                        new Gouteur(),
                        new Gouteur()
                )
        );

        VisiteurConcret visiteurConcret = new VisiteurConcret(10);
        accessoire.accept(visiteurConcret);

        System.out.println(visiteurConcret.getResult());

        // Exemple avec un accessoire composé différement
        Accessoire accessoire1 = new ConnecteurX(
                new ConnecteurX(
                        new Gouteur(),
                        new Gouteur(),
                        new Gouteur()
                ),
                new ConnecteurY(
                        new Gouteur(),
                        new Gouteur()
                ),
                new Gouteur()
        );

        VisiteurConcret v2 = new VisiteurConcret(10);
        accessoire1.accept(v2);

        System.out.println(v2.getResult());
    }
}
