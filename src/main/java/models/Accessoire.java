package models;

import visiteurs.Visitor;

public interface Accessoire {
    void accept(Visitor v);
}
